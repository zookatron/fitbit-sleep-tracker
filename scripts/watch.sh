#!/bin/sh
docker compose -p fitbit-sleep-tracker -f ./scripts/docker-compose.yml up --abort-on-container-exit --build
docker compose -p fitbit-sleep-tracker -f ./scripts/docker-compose.yml rm -fsv
