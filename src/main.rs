use anyhow::anyhow;
use axum::{
    extract::{Path, Query},
    handler::Handler,
    http::Request,
    middleware::{self, Next},
    response::{IntoResponse, Redirect, Response},
    routing::get,
    Router,
};
use log::info;
use serde::Deserialize;
use std::net::SocketAddr;
use templates::*;
use tower_http::services::ServeDir;
use utils::fitbit::*;
use utils::state::*;
use utils::views::*;

mod templates;
mod utils;

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();
    env_logger::init();

    let app = Router::new()
        .route("/", get(dashboard.layer(middleware::from_fn(authorize))))
        .route(
            "/date/:date",
            get(date.layer(middleware::from_fn(authorize))),
        )
        .route("/auth", get(auth))
        .nest_service("/assets", ServeDir::new("assets"))
        .fallback(not_found);

    info!("Server is listening on port 8080");
    axum::Server::bind(&SocketAddr::from(([0, 0, 0, 0], 8080)))
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn authorize<Data>(request: Request<Data>, next: Next<Data>) -> Response {
    let has_data = get_state()
        .await
        .ok()
        .map(|value| !value.access_token.is_empty())
        .unwrap_or(false);
    error::show_errors(
        (async move {
            Ok(match has_data {
                true => next.run(request).await,
                false => Redirect::to(&fitbit_auth_uri().await?).into_response(),
            })
        })
        .await,
    )
}

#[derive(Deserialize)]
struct DashboardParams {
    date: Option<String>,
}
async fn dashboard(params: Query<DashboardParams>) -> Response {
    match &params.date {
        Some(date) => Redirect::to(format!("/date/{0}", date).as_str()).into_response(),
        None => view(dashboard::dashboard, ()),
    }
}

#[derive(Deserialize)]
struct AuthParams {
    code: String,
}
async fn auth(params: Query<AuthParams>) -> Response {
    error::show_errors(
        (async {
            set_state(&State {
                access_token: get_fitbit_access_token(&params.code).await?,
                code_verifier: "".to_string(),
            })
            .await?;
            Ok(Redirect::to("/"))
        })
        .await,
    )
}

async fn date(Path(date): Path<String>) -> Response {
    error::show_errors(
        handle_fitbit_error(
            (async {
                let sleep = fitbit_api(
                    "/1.2/user/-/sleep/list.json",
                    vec![
                        ("afterDate", &date),
                        ("sort", "asc"),
                        ("offset", "0"),
                        ("limit", "1"),
                    ],
                )
                .await?;
                let spo2 =
                    fitbit_api(&format!("/1/user/-/spo2/date/{0}/all.json", date), vec![]).await?;
                let heart_rate =
                    fitbit_api(&format!("/1/user/-/hrv/date/{0}/all.json", date), vec![]).await?;
                Ok(view(
                    date::date,
                    date::DateProps {
                        date,
                        data: async {
                            let sleep_summary = sleep
                                .get("sleep")?
                                .as_array()?
                                .get(0)?
                                .get("levels")?
                                .get("summary");
                            let empty_spo2_minutes = vec![];
                            let spo2_minutes = spo2
                                .get("minutes")?
                                .as_array()
                                .unwrap_or(&empty_spo2_minutes)
                                .iter()
                                .map(|minute| minute.get("value")?.as_f64())
                                .map(|minute| minute.unwrap_or(0.0));
                            let spo2_mean =
                                spo2_minutes.clone().reduce(|sum, value| sum + value)?
                                    / spo2_minutes.len() as f64;
                            let spo2_variance = spo2_minutes.clone().fold(0.0, |sum, value| {
                                sum + (value - spo2_mean) * (value - spo2_mean)
                            }) / (spo2_minutes.len() - 1) as f64;
                            let empty_heart_rate_minutes = vec![];
                            let heart_rate_minutes = heart_rate
                                .get("hrv")?
                                .as_array()?
                                .get(0)?
                                .get("minutes")?
                                .as_array()
                                .unwrap_or(&empty_heart_rate_minutes)
                                .iter()
                                .map(|minute| minute.get("value"));
                            let heart_rmssd =
                                heart_rate_minutes.clone().fold(Some(0.0), |sum, value| {
                                    Some(sum? + value?.get("rmssd")?.as_f64()?)
                                })? / heart_rate_minutes.len() as f64;
                            let heart_hff =
                                heart_rate_minutes.clone().fold(Some(0.0), |sum, value| {
                                    Some(sum? + value?.get("hf")?.as_f64()?)
                                })? / heart_rate_minutes.len() as f64;
                            let heart_lff =
                                heart_rate_minutes.clone().fold(Some(0.0), |sum, value| {
                                    Some(sum? + value?.get("lf")?.as_f64()?)
                                })? / heart_rate_minutes.len() as f64;
                            Some(vec![
                                ("Sleep score".to_string(), "?".to_string()),
                                (
                                    "Light sleep time".to_string(),
                                    format!(
                                        "{:.2}",
                                        sleep_summary?.get("light")?.get("minutes")?.as_f64()?
                                            / 60.0
                                    ),
                                ),
                                (
                                    "Deep sleep time".to_string(),
                                    format!(
                                        "{:.2}",
                                        sleep_summary?.get("deep")?.get("minutes")?.as_f64()?
                                            / 60.0
                                    ),
                                ),
                                (
                                    "REM time".to_string(),
                                    format!(
                                        "{:.2}",
                                        sleep_summary?.get("rem")?.get("minutes")?.as_f64()? / 60.0
                                    ),
                                ),
                                (
                                    "Average blood oxygen".to_string(),
                                    format!("{:.2}", spo2_mean),
                                ),
                                (
                                    "Blood oxygen variance".to_string(),
                                    format!("{:.2}", spo2_variance),
                                ),
                                (
                                    "Heart rate RMSSD".to_string(),
                                    format!("{:.2}", heart_rmssd),
                                ),
                                ("Heart rate HFF".to_string(), format!("{:.2}", heart_hff)),
                                ("Heart rate LFF".to_string(), format!("{:.2}", heart_lff)),
                                (
                                    "Wake Count".to_string(),
                                    format!(
                                        "{:.0}",
                                        sleep_summary?.get("wake")?.get("count")?.as_f64()?
                                    ),
                                ),
                            ])
                        }
                        .await
                        .ok_or(anyhow!("Invalid sleep data returned by Fitbit!"))?,
                    },
                ))
            })
            .await,
        )
        .await,
    )
}

async fn not_found() -> Response {
    view(
        error::error,
        error::ErrorProps {
            error: "404 Not Found".to_string(),
        },
    )
}
