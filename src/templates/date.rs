use dioxus::prelude::*;

#[derive(Props, PartialEq)]
pub struct DateProps {
    pub date: String,
    pub data: Vec<(String, String)>,
}

pub fn date(cx: Scope<DateProps>) -> Element {
    render!(rsx! {
        super::page::page {
            h1 {
                class: "text-xl mb-3",
                cx.props.date.clone()
            }
            div {
                class: "relative",
                div {
                    class: "absolute bg-white dark:bg-black w-full h-full top-0 left-0 rounded-lg drop-shadow-md -z-10",
                }
                table {
                    class: "w-full text-left text-sm text-gray-500 dark:text-gray-400",
                    thead {
                        class: "group/head text-xs uppercase text-gray-700 dark:text-gray-400",
                        tr {
                            cx.props.data.iter().map(|(key, _value)| rsx! {
                                th {
                                    class: "group-first/head:first:rounded-tl-lg group-first/head:last:rounded-tr-lg bg-gray-50 dark:bg-gray-700 px-6 py-3",
                                    key.clone()
                                }
                            })
                        }
                    }
                    tbody {
                        class: "group/body",
                        tr {
                            class: "group/row",
                            cx.props.data.iter().map(|(_key, value)| rsx! {
                                td {
                                    class: "group-first/body:group-first/row:first:rounded-tl-lg group-first/body:group-first/row:last:rounded-tr-lg group-last/body:group-last/row:first:rounded-bl-lg group-last/body:group-last/row:last:rounded-br-lg px-6 py-4",
                                    value.clone(),
                                }
                            })
                        }
                    }
                }
            }
        }
    })
}
