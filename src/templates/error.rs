use super::super::utils::views::view;
use anyhow::Result;
use axum::response::{IntoResponse, Response};
use dioxus::prelude::*;

#[derive(Props, PartialEq)]
pub struct ErrorProps {
    pub error: String,
}

pub fn error(cx: Scope<ErrorProps>) -> Element {
    render!(rsx! {
      super::page::page {
        layout: "simple",
        div {
          role: "alert",
          class: "flex flex-col gap-2 p-4 text-sm text-red-700 bg-red-100 border-red-500 dark:bg-red-200 dark:text-red-800 rounded-lg",
          div {
            class: "flex items-center",
            div { cx.props.error.clone() }
          }
        }
      }
    })
}

pub fn show_errors<IntoResp>(result: Result<IntoResp>) -> Response
where
    IntoResp: IntoResponse,
{
    match result {
        Ok(response) => response.into_response(),
        Err(err) => view(
            error,
            ErrorProps {
                error: err.to_string(),
            },
        ),
    }
}
