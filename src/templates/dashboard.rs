use dioxus::prelude::*;

pub fn dashboard(cx: Scope) -> Element {
    render!(rsx! {
      super::page::page {
        form {
          class: "flex flex-col gap-4",
          div {
            div {
              class: "mb-2 block",
              label {
                r#for: "date",
                class: "text-sm font-medium text-gray-900 dark:text-gray-300",
                "Choose the date you want to view:"
              }
            }
            div {
              class: "flex",
              div {
                class: "relative w-full",
                input {
                  id: "date",
                  required: true,
                  name: "date",
                  r#type: "date",
                  class: "block w-full border disabled:cursor-not-allowed disabled:opacity-50 bg-gray-50 border-gray-300 text-gray-900 focus:border-cyan-500 focus:ring-cyan-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-cyan-500 dark:focus:ring-cyan-500 rounded-lg p-2.5 text-sm",
                }
              }
            }
          }
          button {
            r#type: "submit",
            class: "text-white bg-cyan-700 border border-transparent hover:bg-cyan-800 focus:ring-4 focus:ring-cyan-300 disabled:hover:bg-cyan-700 dark:bg-cyan-600 dark:hover:bg-cyan-700 dark:focus:ring-cyan-800 dark:disabled:hover:bg-cyan-600 focus:!ring-2 group flex h-min items-center justify-center p-0.5 text-center font-medium focus:z-10 rounded-lg",
            span {
              class: "flex items-center rounded-md text-sm px-4 py-2",
              "Submit"
            }
          }
        }
      }
    })
}
