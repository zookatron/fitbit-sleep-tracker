use dioxus::prelude::*;

#[derive(Props)]
pub struct PageProps<'page_life> {
    layout: Option<&'page_life str>,
    children: Element<'page_life>,
}

pub fn page<'page_life>(
    cx: &'page_life Scoped<'page_life, PageProps<'page_life>>,
) -> Element<'page_life> {
    let layout = cx.props.layout.unwrap_or("default");
    render!(rsx! {
      head {
        meta { charset: "utf-8" }
        title { "Fitbit Sleep Tracker" }
        meta { name: "viewport", content: "width=device-width, initial-scale=1" }
        meta { name: "description", content: "" }
        link { rel: "icon", href: "/assets/logo.svg", r#type: "image/svg+xml" }
        link { rel: "manifest", href: "/assets/manifest.webmanifest" }
        link { rel: "stylesheet", href: "/assets/style.css" }
      }
      body {
        div {
          class: "mx-auto flex max-w-2xl p-10 min-h-screen items-center justify-center",
          match layout {
            "simple" => rsx! { &cx.props.children },
            _ => rsx! { div {
              class: "flex rounded-lg border border-gray-200 bg-white shadow-md dark:border-gray-700 dark:bg-gray-800 flex-col grow",
              div {
                class: "flex h-full flex-col justify-center gap-4 p-6",
                &cx.props.children,
              }
            } },
          }
        }
      }
    })
}
