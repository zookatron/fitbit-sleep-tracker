use anyhow::Result;
use serde::{Deserialize, Serialize};
use serde_json::from_str;
use serde_json::json;
use tokio::fs::read_to_string;
use tokio::fs::remove_file;
use tokio::fs::write;

#[derive(Serialize, Deserialize)]
pub struct State {
    pub code_verifier: String,
    pub access_token: String,
}

pub async fn get_state() -> Result<State> {
    Ok(from_str(&read_to_string("state.json").await?)?)
}

pub async fn set_state(state: &State) -> Result<()> {
    Ok(write("state.json", json!(state).to_string()).await?)
}

pub async fn clear_state() -> Result<()> {
    Ok(remove_file("state.json").await?)
}
