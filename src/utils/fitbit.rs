use super::state::*;
use anyhow::Result;
use axum::response::{IntoResponse, Redirect, Response};
use base64::{engine, Engine};
use rand::distributions::Alphanumeric;
use rand::Rng;
use serde::Deserialize;
use serde_json::Value;
use sha2::{Digest, Sha256};

#[derive(Debug)]
struct FitbitError {
    pub message: String,
    pub error_type: String,
}

impl std::fmt::Display for FitbitError {
    fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        formatter.write_str(&format!(
            "Fitbit Error: {0}: {1}",
            self.message, self.error_type
        ))?;
        Ok(())
    }
}

impl std::error::Error for FitbitError {}

pub async fn fitbit_api(url: &str, params: Vec<(&str, &str)>) -> Result<Value> {
    let state = get_state().await?;
    let param_str = params
        .iter()
        .map(|param| format!("{0}={1}", param.0, param.1))
        .collect::<Vec<_>>()
        .join("&");
    let text = reqwest::Client::new()
        .get(format!("https://api.fitbit.com{0}?{1}", url, param_str))
        .header("authorization", format!("Bearer {0}", state.access_token))
        .send()
        .await?
        .text()
        .await?;
    let json: Value = serde_json::from_str(&text)?;
    if !json
        .get("success")
        .and_then(|value| value.as_bool())
        .unwrap_or(true)
    {
        return Err(async {
            let error = json.get("errors")?.as_array()?.get(0)?.as_object();
            Some(FitbitError {
                message: error?.get("message")?.as_str()?.to_string(),
                error_type: error?.get("errorType")?.as_str()?.to_string(),
            })
        }
        .await
        .unwrap_or(FitbitError {
            message: "Unknown error".to_string(),
            error_type: "unknown".to_string(),
        })
        .into());
    }
    Ok(json)
}

pub async fn handle_fitbit_error<IntoResp>(result: Result<IntoResp>) -> Result<Response>
where
    IntoResp: IntoResponse,
{
    match result {
        Ok(response) => Ok(response.into_response()),
        Err(error) => match error.downcast_ref::<FitbitError>() {
            Some(fitbit_error) => match fitbit_error.error_type.as_ref() {
                "invalid_token" => clear_state()
                    .await
                    .map(|_| Redirect::to("/").into_response()),
                "expired_token" => clear_state()
                    .await
                    .map(|_| Redirect::to("/").into_response()),
                _ => Err(error),
            },
            None => Err(error),
        },
    }
}

pub async fn fitbit_auth_uri() -> Result<String> {
    let state = match get_state().await {
        Ok(loaded) => loaded,
        Err(_) => {
            let new_state = State {
                code_verifier: rand::thread_rng()
                    .sample_iter(&Alphanumeric)
                    .take(128)
                    .map(char::from)
                    .collect(),
                access_token: "".to_string(),
            };
            set_state(&new_state).await?;
            new_state
        }
    };
    let scopes = [
        "activity",
        "cardio_fitness",
        "electrocardiogram",
        "heartrate",
        "location",
        "nutrition",
        "oxygen_saturation",
        "profile",
        "respiratory_rate",
        "settings",
        "sleep",
        "social",
        "temperature",
        "weight",
    ];
    let mut hasher = Sha256::new();
    hasher.update(state.code_verifier);
    let params = [
        ["response_type", "code"],
        [
            "code_challenge",
            &engine::general_purpose::URL_SAFE_NO_PAD.encode(hasher.finalize()),
        ],
        ["code_challenge_method", "S256"],
        [
            "client_id",
            std::env::var("FITBIT_CLIENT_ID")
                .expect("No Fitbit API client ID provided!")
                .as_str(),
        ],
        ["redirect_uri", "http://localhost:8080/auth"],
        ["scope", scopes.join(" ").as_str()],
    ]
    .map(|param| param.map(urlencoding::encode).join("="))
    .join("&");
    Ok(format!(
        "https://www.fitbit.com/oauth2/authorize?{0}",
        params
    ))
}

#[derive(Deserialize)]
struct AccessCodeResponse {
    access_token: String,
}

pub async fn get_fitbit_access_token(code: &str) -> Result<String> {
    let state = get_state().await?;
    let text = reqwest::Client::new()
        .post("https://api.fitbit.com/oauth2/token")
        .header(
            "authorization",
            format!(
                "Basic {0}",
                engine::general_purpose::STANDARD.encode(format!(
                    "{0}:{1}",
                    std::env::var("FITBIT_CLIENT_ID").expect("No Fitbit API client ID provided!"),
                    std::env::var("FITBIT_CLIENT_SECRET")
                        .expect("No Fitbit API client secret provided!"),
                ))
            ),
        )
        .header("content-type", "application/x-www-form-urlencoded")
        .body(
            [
                (
                    "client_id",
                    std::env::var("FITBIT_CLIENT_ID")
                        .expect("No Fitbit API client ID provided!")
                        .as_ref(),
                ),
                ("code", code),
                ("code_verifier", &state.code_verifier),
                ("grant_type", "authorization_code"),
                ("redirect_uri", "http://localhost:8080/auth"),
            ]
            .map(|param| format!("{0}={1}", param.0, param.1))
            .join("&"),
        )
        .send()
        .await?
        .text()
        .await?;
    let json: AccessCodeResponse = serde_json::from_str(&text)?;
    Ok(json.access_token)
}
