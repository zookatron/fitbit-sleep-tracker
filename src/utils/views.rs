use axum::response::{Html, IntoResponse, Response};
use dioxus::prelude::*;

pub fn view<Props: 'static>(element: fn(Scope<Props>) -> Element, props: Props) -> Response {
    let mut dom = VirtualDom::new_with_props(element, props);
    let _ = dom.rebuild();
    Html(format!(
        "<!doctype html><html lang=\"en-US\">{0}</html>",
        dioxus_ssr::render(&dom)
    ))
    .into_response()
}
