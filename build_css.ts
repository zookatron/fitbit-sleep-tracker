#!/usr/bin/env -S deno run --no-prompt --allow-read --allow-write --allow-net
import {
  dirname,
  fromFileUrl,
} from "https://deno.land/std@0.192.0/path/mod.ts";
import { UserConfig } from "npm:@unocss/core@0.53.4";
import { build as unocssBuild } from "npm:@unocss/cli@0.53.4";
import presetWind from "npm:@unocss/preset-wind@0.53.4";
import { presetFlowbite } from "npm:@julr/unocss-preset-flowbite@0.0.2";

async function unocss(
  outFile: string,
  patterns: string[],
  options?: UserConfig,
) {
  const preflightUrl =
    "https://raw.githubusercontent.com/unocss/unocss/v0.53.4/packages/reset/tailwind.css";
  const preflight = fetch(preflightUrl).then((response) =>
    response.ok ? response.text() : Promise.reject("Unable to fetch preflight")
  );
  await unocssBuild({
    outFile,
    patterns,
    config: {
      ...options,
      presets: [...(options?.presets || []), presetWind(), presetFlowbite()],
    } as unknown as string,
  });
  await Deno.writeTextFile(
    outFile,
    (await preflight).concat(await Deno.readTextFile(outFile)),
  );
}

if (import.meta.main) {
  const directory = dirname(fromFileUrl(import.meta.url));
  await unocss(`${directory}/assets/style.css`, [`${directory}/src/**/*.rs`]);
}
