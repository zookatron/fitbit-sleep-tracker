# Fitbit Sleep Tracker

This is a small web application built with Rust intended to be used to easily access Fitbit sleep data via the Fitbit API.

![First Screenshot](screenshots/screenshot1.jpg "First Screenshot")
![Second Screenshot](screenshots/screenshot2.jpg "Second Screenshot")
![Third Screenshot](screenshots/screenshot3.jpg "Third Screenshot")
